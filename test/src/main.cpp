//
// Created by main on 1/3/19.
//

#include <string>
#include <iostream>
#if(__MINGW32__)

#include <mingw.thread.h>
#include <mingw.mutex.h>
#include <mingw.condition_variable.h>

#endif
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include "RedAlder/AddonFramework/Instance.h"

int main() {
    auto stdoutSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    auto fileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
            "runtime/logs/general.log", false);

    Instance instance = Instance("runtime/addons", "runtime/mount", std::make_shared<spdlog::logger>("addonframework",
            std::initializer_list<std::shared_ptr<spdlog::sinks::sink>>{stdoutSink, fileSink}));
}
