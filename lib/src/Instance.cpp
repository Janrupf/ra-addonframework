//
// Created by main on 1/2/19.
//
#if (__MINGW32__)
#
#include <experimental/filesystem>

namespace std::filesystem {
    using namespace std::experimental::filesystem;
};
#else
#include <filesystem>
#endif
#include <dlfcn.h>
#include <yaml-cpp/yaml.h>
#include <RedAlder/AddonFramework/Instance.h>


#include "RedAlder/PlatformDecypher.h"
#include "RedAlder/AddonFramework/Instance.h"

Instance::Instance(std::string addonsDir, std::string mountsDir, std::shared_ptr<spdlog::logger> logger)
    : _mountsDir(std::move(mountsDir)), _addonsDir(std::move(addonsDir)), _logger(std::move(logger)) {
    if (!std::filesystem::exists(_addonsDir)) {
        std::filesystem::create_directories(_addonsDir);
    } else if (std::filesystem::exists(_addonsDir) && !std::filesystem::is_directory(_addonsDir)) {
        _logger->error("Addons path: '{}' already exists but it's not a directory", _addonsDir);
        exit(EXIT_FAILURE);
    }

    if (!std::filesystem::exists(_mountsDir)) {
        std::filesystem::create_directories(_mountsDir);
    } else if (std::filesystem::exists(_mountsDir) && !std::filesystem::is_directory(_mountsDir)) {
        _logger->error("Mounts path: '{}' already exists but it's not a directory", _mountsDir);
        exit(EXIT_FAILURE);
    }

    for (auto& file : std::filesystem::directory_iterator(_addonsDir)) {
#if (__MINGW32__)
        if (std::filesystem::is_regular_file(file.path()) && file.path().extension().string() == ".zip") {
#else
        if (file.is_regular_file() && file.path().extension().string() == ".zip") {
#endif
            std::filesystem::create_directory(_mountsDir + "/" + file.path().filename().string());
            mountZipFile(file.path().string(), _mountsDir + "/" + file.path().filename().string());

            loadAddon(_mountsDir + "/" + file.path().filename().string());
        }
    }

    for (auto& addon : LoadedAddons) {
        loadLibrary(addon.second);
    }
}

Instance::Instance(const char* addonsDir, const char* mountsDir, std::shared_ptr<spdlog::logger> logger)
    : Instance(std::string(addonsDir), std::string(mountsDir), std::move(logger)) {}

Instance::Instance(char* addonsDir, char* mountsDir, std::shared_ptr<spdlog::logger> logger)
    : Instance(std::string(addonsDir), std::string(mountsDir), std::move(logger)) {}

Instance::~Instance() {
    umountZipFiles();
    unloadLibraries();
}

void Instance::loadAddon(std::string addonDir) {
    YAML::Node addonConfig;
    try {
        addonConfig = YAML::LoadFile(addonDir + "/addon_config.yaml");
    } catch (const YAML::BadFile& ex) {
        _logger->error("'{}/addon_config.yaml' not found", addonDir);
        return;
    }



    if (addonConfig["addons"]) {
        for (YAML::Node node : addonConfig["addons"]) {
            if (node["name"] && node["version"] && node["architectures"] && node["resource_location"]) {
                std::string resourceLocation;

                if (!node["architectures"][DecypherPlatform()]) {
                    _logger->error("Addon '{}' doesn't support '{}'", node["name"].as<std::string>(), DecypherPlatform());
                    return;
                }

                Addon addon = Addon {
                    nullptr,
                    AddonInfo {
                        node["name"].as<std::string>(),
                        node["resource_location"].as<std::string>(),
                        node["architectures"][DecypherPlatform()].as<std::string>(),
                        AddonVersion(node["version"].as<std::string>())
                    }
                };

                LoadedAddons.insert(std::pair<std::string, Addon>(node["name"].as<std::string>(), addon));
            } else {
                _logger->error("'{}/addon-config.yaml' is invalid", addonDir);
            }
        }
    } else {
        _logger->error("'addons' node not found in '{}/addon_config.yaml'", addonDir);
        return;
    }
}

void Instance::loadLibrary(Addon& addon) {
    std::string dynamicLib = addon.Info.DynamicLibraryLocation;

    if (!std::filesystem::exists(dynamicLib) || std::filesystem::is_regular_file(dynamicLib)) {
        _logger->error("'{}' not found", dynamicLib);
        return;
    }

    addon.SharedLibrary = dlopen(dynamicLib.c_str(), RTLD_GLOBAL);
}

void Instance::unloadLibraries() {
    for (auto& addon : LoadedAddons) {
        if (addon.second.SharedLibrary != nullptr) {
            dlclose(addon.second.SharedLibrary);
        }
    }
}

void Instance::mountZipFile(std::string zipFile, std::string mountPoint) {
    system(("fuse-zip -o ro " + zipFile + " " + mountPoint).c_str());
}

void Instance::umountZipFiles() {
    for (auto& directory : std::filesystem::directory_iterator(_mountsDir)) {
#if (__MINGW32__)
        if (std::filesystem::is_directory(directory.path()) && directory.path().extension().string() == ".zip") {
#else
        if (directory.is_directory() && directory.path().extension().string() == ".zip") {
#endif
            system(("fusermount -u " + directory.path().string()).c_str());
            std::filesystem::remove(directory.path().string());
        }
    }
}
